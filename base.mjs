var lnwrap = document.createElement("button");
lnwrap.textContent = "re-wrap lines";
lnwrap.style.display = "block";
lnwrap.addEventListener("click", () => {
	document.body.style.width = String(document.documentElement.clientWidth) + "px";
});
document.body.insertBefore(lnwrap, document.body.firstChild);
document.body.style.width = String(Math.min(768, document.documentElement.clientWidth)) + "px";