"use strict";

var {
	fs,
	crypto,
} = new Proxy({}, {get: (_, x) => require(x)});

var baseurl = "https://dou4cc.gitlab.io/";

var file_code_to_lns = (x) => fs
	.readFileSync("." + x, "UTF-8")
	.replace(/^\u{FEFF}/su, "")
	.split(/\r\n?|\n/gsu)
	.filter((ln) => ln && !ln.startsWith("#"))
;

var file_post_to_html = (...x) => [...(function* recur(file, seg_id){
	var lns = file_code_to_lns(file + ".post");
	seg_id = seg_id == "*" ? null : seg_id;
	while(true){
		if(!lns.length)break;
		while(seg_id && lns.length && lns.shift() != "start " + seg_id);
		for(
			; lns.length && !(seg_id && lns[0] == "stop " + seg_id)
			; lns.shift()
		){
			if(lns[0].startsWith("\t"))yield lns[0].slice(1);
			if(lns[0].startsWith("/"))yield baseurl + encodeURI(lns[0].slice(1));
			if(lns[0].startsWith("quote ")){
				var [, quoted_seg_id, quoted_file] = lns[0].match(/ (\S+) (.+)/su);
				yield* recur(quoted_file, quoted_seg_id);
			}
			if(lns[0].startsWith("list "))yield* file_to_ents(lns[0].slice(5)).map(({url, tit}) =>
				'<li><a href="' + url + '">' + tit + '</a></li>');
		}
	}
})(...x)].join("\n");

var xmlescing = (x) => x
	.replace(/&/gsu, "&amp;")
	.replace(/</gsu, "&lt;")
;

var file_to_ents = function recur(file, wildcards = [""]){
	try{
		return file_code_to_lns(file + ".ents")
			.map((x) => x.match(/(?:(\d+) )?(.+)/su))
			.filter(([, flag]) =>
				wildcards.some((x) =>
					[...x].every((bit, cur) =>
						bit == "0" || flag?.[cur] == "1")))
			.map(([,, file]) => ({
				url: baseurl + encodeURI(file.slice(1) + ".html")
					.replace(/#/gsu, "%23")
					.replace(/\?/gsu, "%3F")
				,
				tit: file_post_to_html(file, "tit"),
				_static: file_post_to_html(file, "static"),
			}))
		;
	}catch(_){}
	var [file, ...wildcards] = file_code_to_lns(file + ".entsflt");
	return recur(file + ".ents", wildcards.length ? wildcards : undefined);
};

(function recur(dir){
	try{
		for(var file of fs.readdirSync(dir)){
			file = dir + file;
			if(fs.lstatSync(file).isDirectory()){
				recur(file + "/");
				continue;
			}
			[file] = file.match(/.*(?=\.[^.]*$)|.*/su);
			for(var [ext, compiling] of Object.entries({
				html: (file) =>
					'<!DOCTYPE html>\n' +
					'<html>\n' +
					'\n' +
					'<head>\n' +
					'\t<meta charset="UTF-8">\n' +
					'\t<title>' + file_post_to_html(file, "tit") + '</title>\n' +
					'\t<meta name="referrer" content="no-referrer">\n' +
					'\t<style>\n' +
					'\t\t@import url("' + baseurl + 'base.css");\n' +
					'\t</style>\n' +
					'\t<script type="module">\n' +
					'\t\timport {} from "' + baseurl + 'base.mjs";\n' +
					'\t</script>\n' +
					'\t<meta name="viewport" content="width=device-width">\n' +
					'</head>\n' +
					'\n' +
					'<body>\n' +
					file_post_to_html(file) + '\n' +
					'</body>\n' +
					'\n' +
					'</html>',
				atom: (file) =>
					'<?xml version="1.0"?>' +
					'<feed xmlns="http://www.w3.org/2005/Atom">' +
					file_to_ents(file)
						.map(({url, tit, _static}) => {
							var code =
								'<link href="' + url + '"/>' +
								'<title type="html">' +
								xmlescing(tit) +
								'</title>' +
								(_static &&
									'<summary type="html">' +
									xmlescing(_static) +
									'</summary>');
							return '<entry>' +
								'<id>' + crypto
									.createHash("SHA224")
									.update(code)
									.digest("base64")
									.slice(0, -2)
								+ '</id>' +
								code +
								'</entry>';
						})
						.join("")
					+
					'</feed>',
			})){
				var dstn = file + "." + ext;
				if(fs.existsSync(dstn))continue;
				try{
					fs.writeFileSync(dstn, compiling(file.slice(1)));
				}catch(_){}
			}
		}
	}catch(_){}
})("./");